// User data model

function UserFactory(userInput) {
  return Object.freeze(new User(userInput));
}
class User {
  constructor({ name, email, ...rest }) {
    if (typeof name !== "string") {
      throw new Error("validation fail");
    }

    if (name.length < 1 || name.length > 30) {
      throw new Error("validation fail");
    }

    this.name = name;

    if (typeof email !== "string") {
      throw new Error("validation fail");
    }

    if (email.length < 3 || email.length > 90) {
      throw new Error("validation fail");
    }

    this.email = email;
    this.id = auth_user_id();
    this.role = "user";

    if (rest !== undefined && Object.keys(rest).length !== 0) {
      throw new Error("additional properties detected");
    }
  }
}

// A psudo authenticator
// returns authenticated user ID
function auth_user_id() {
  return 2;
}

module.exports = { auth_user_id, UserFactory };
